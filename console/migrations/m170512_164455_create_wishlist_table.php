<?php

use yii\db\Migration;


class m170512_164455_create_wishlist_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('wishlist', [
            'id' =>'pk',
            'created_at'=>$this->integer()->notNull(),
	        'updated_at'=>$this->integer()->notNull(),
	        'advert_id'=>$this->integer(11)->notNull(),
	        'user_id'=>$this->integer(11)->notNull()
        ]);

       $this->addForeignKey('wishlist_user', 'wishlist', 'user_id', 'user', 'id');
        $this->addForeignKey('wishlist_advert', 'wishlist', 'advert_id', 'advert', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('wishlist_user', 'wishlist');
        $this->dropForeignKey('wishlist_advert', 'wishlist');
        $this->dropTable('wishlist');
    }
}
