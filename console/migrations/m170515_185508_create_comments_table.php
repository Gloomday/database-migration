<?php

use yii\db\Migration;


class m170515_185508_create_comments_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('comments', [
            'id' =>'pk',
            'description' => $this->text()->notNull(),
	        'created_at'=>$this-> integer()->notNull(),
	        'user_id'=>$this->integer(11)->notNull(),
	        'advert_id'=>$this->integer(11)->notNull()
        ]);

        $this->addForeignKey('comments_user', 'comments', 'user_id', 'user', 'id');
        $this->addForeignKey('comments_advert', 'comments', 'advert_id', 'advert', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('comments_user', 'comments');
        $this->dropForeignKey('comments_advert', 'comments');
        $this->dropTable('comments');
    }
}
